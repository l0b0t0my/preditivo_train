from string import Template
from src.config.config import ETL_INPUT_TABLE


def get_keys_from_dict_as_sql_values(dictionary):
    return ', '.join(f'({str(p)})' for p in dictionary.keys())


def get_keys_from_dict_as_list(dictionary):
    return ', '.join(f'{str(p)}' for p in dictionary.keys())


procedimentos_bariatrica = {
    533470: 'pressao_arterial',
    536273: 'glicemia',
    537142: 'glicemia',
    536230: 'colesterol',
    536228: 'colesterol',
    536229: 'colesterol',
    536338: 'colesterol',
    536344: 'colesterol',
    536323: 'colesterol',
    550431: 'funcao_cardiaca',
    538306: 'funcao_cardiaca',
    550464: 'funcao_cardiaca',
    538616: 'funcao_cardiaca',
    535940: 'funcao_cardiaca',
    535942: 'funcao_cardiaca',
    535943: 'funcao_cardiaca',
    536101: 'endoscopia',
    550333: 'endoscopia',
    535930: 'endoscopia',
    536154: 'endoscopia',
    535929: 'endoscopia',
    429716: 'endoscopia',
    550068: 'endoscopia',
    536072: 'endoscopia',
    536071: 'endoscopia',
    550160: 'endoscopia',
    538309: 'ecografia_abdominal',
    538308: 'ecografia_abdominal',
    538453: 'ecografia_abdominal',
    533570: 'avaliacao_psicologica',
    538737: 'avaliacao_psicologica',
    538736: 'avaliacao_psicologica',
    538746: 'avaliacao_nutricional',
    538759: 'avaliacao_nutricional',
    538550: 'bioimpedancia'
}

procedimentos_neonatal = {
    #HIPERTENSÃO
    538301: 'Ecodopplercardiograma',
    538302: 'Ecodopplercardiograma',
    538303: 'Ecodopplercardiograma',
    538304: 'Ecodopplercardiograma',
    538381: 'Ecodopplercardiograma',
    538364: 'Ecodopplercardiograma',
    538363: 'Ecodopplercardiograma',
    538305: 'Ecodopplercardiograma',
    538306: 'Ecodopplercardiograma',
    535940: 'Ecodopplercardiograma',
    538356: 'Ecodopplercardiograma',
    538357: 'Ecodopplercardiograma',
    538358: 'Ecodopplercardiograma',
    538359: 'Ecodopplercardiograma',
    538355: 'Ecodopplercardiograma',
    538362: 'Ecodopplercardiograma',
    #EXAMES DE SANGUE
    536276: 'Hemoglobina',
    536342: 'Hemoglobina',
    #EXAMES OBSTÉTRICOS
    538318: 'USG_obstetrica',
    538343: 'USG_obstetrica',
    538320: 'USG_obstetrica',
    538323: 'USG_obstetrica',
    538370: 'USG_obstetrica',
    533609: 'USG_obstetrica',
    # DIABETES
    536273: 'Diabetes',
    536238: 'Diabetes',
    # CONDIÇÕES GENÉTICAS
    533442: 'Aconselhamento genético',
    #GRAVIDEZ MÚLTIPLA
    538321: 'US - Obstétrica gestação múltipla',
    538322: 'US - Obstétrica gestação múltipla'
}

procedimentos_bucomaxilo = {
    #Diagnostico anaotmopatologico
    534142: 'diag anatomopatologico',
    538769: 'diag anatomopatologico',
    538771: 'diag anatomopatologico',
    538828: 'diag anatomopatologico',
    533684: 'diag anatomopatologico',
    538768: 'diag anatomopatologico',
    538818: 'diag anatomopatologico',
    #Ressonância magnética
    538446: 'RM',
    538447: 'RM',
    #Raio-X
    538027: 'RX',
    538025: 'RX',
    538028: 'RX',
    538787: 'RX',
    538788: 'RX',
    538030: 'RX',
    538032: 'RX',
    538029: 'RX',
    #Tomografia
    538385: 'TC',
    538384: 'TC',
    538408: 'TC',
    538407: 'TC',
}

procedimentos_cardio = {
    #Angio ressonância magnética
    538483:	'angio_rm',
    538489:	'angio_rm',
    538491:	'angio_rm',
    538468:	'angio_rm',
    538484:	'angio_rm',
    538490:	'angio_rm',
    538492:	'angio_rm',
    #Angiografia
    538547:	'angiografia',
    538548:	'angiografia',
    538181:	'angiografia',
    852340:	'angiografia',
    538168:	'angiografia',
    537929:	'angiografia',
    #Angiotomografia
    538417:	'angiotomografia',
    538425:	'angiotomografia',
    538419:	'angiotomografia',
    538421:	'angiotomografia',
    538398:	'angiotomografia',
    538397:	'angiotomografia',
    538418:	'angiotomografia',
    538422:	'angiotomografia',
    538403:	'angiotomografia',
    #Colesterol
    536230:	'Colesterol',
    536228:	'Colesterol',
    536229:	'Colesterol',
    536323:	'Colesterol',
    536338:	'Colesterol',
    536344:	'Colesterol',
    #Exames - anatomia e funcionalidade do coração
    538389:	'Coração',
    538083:	'Coração',
    538451:	'Coração',
    #Eletrocardiograma
    535940:	'ECG',
    535941:	'ECG',
    #Ecodoppler
    538303:	'ecodopler',
    538363:	'ecodopler',
    538306:	'ecodopler',
    538356:	'ecodoppler',
    538362:	'ecodoppler',
    538305:	'ecodoppler',
    538301:	'ecodoppler',
    538359:	'ecodoppler',
    538381:	'ecodoppler',
    #Holter
    533477:	'holter',
    533468:	'holter',
    533469:	'holter',
    533473:	'holter',
    533474:	'holter',
    533475:	'holter',
    #Teste ergométrico
    535942:	'teste_ergometrico',
    535943:	'teste_ergometrico',
    535945:	'teste_ergometrico',
    538617:	'teste_ergometrico',
    538616:	'teste_ergometrico',
    #Diabetes
    536273:	'Diabetes',
    536505:	'Diabetes',
    537178:	'Diabetes',
    537421:	'Diabetes',
    537235:	'Diabetes',
    536506:	'Diabetes',
    537228:	'Diabetes',
    537229:	'Diabetes',
    537230:	'Diabetes',
    537231:	'Diabetes',
    537240:	'Diabetes',
    537232:	'Diabetes',
    537233:	'Diabetes',
    537234:	'Diabetes',
    537227:	'Diabetes',
}

procedimentos_ortopedia = {
    # TC
    538385: 'TC',
    538382: 'TC',
    538384: 'TC',
    538388: 'TC',
    538393: 'TC ',
    538392: 'TC',
    538394: 'TC',
    538395: 'TC',
    538396: 'TC',
    # RM
    538464: 'RM',
    538467: 'RM',
    538466: 'RM',
    538458: 'RM',
    538438: 'RM',
    538454: 'RM',
    538445: 'RM',
    538461: 'RM',
    538465: 'RM',
    538463: 'RM',
    538462: 'RM',
    538449: 'RM',
    538446: 'RM',
    538440: 'RM',
    # FISIOTERAPIA
    538706: 'fisioterapia',
    538704: 'fisioterapia',
    550369: 'fisioterapia',
    # PATOLOGIA
    533527: 'Patologia osteomioarticular',
    533525: 'Patologia osteomioarticular',
    533526: 'Patologia osteomioarticular',
    533528: 'Patologia osteomioarticular',
    # RX
    538074: 'RX',
    538044: 'RX',
    538057: 'RX',
    538061: 'RX',
    538079: 'RX',
    538069: 'RX',
    538070: 'RX',
    538060: 'RX',
    538058: 'RX',
    538073: 'RX',
    538080: 'RX',
    538059: 'RX',
    538040: 'RX',
    538054: 'RX',
    538071: 'RX',
    538042: 'RX',
    538062: 'RX',
    538045: 'RX',
    538082: 'RX',
    538053: 'RX',
    538046: 'RX',
    538068: 'RX',
    538075: 'RX'
}

procedimentos_by_model = {
    'bariatrica': procedimentos_bariatrica,
    'neonatal': procedimentos_neonatal,
    'ortopedia': procedimentos_ortopedia,
    'bucomaxilo': procedimentos_bucomaxilo,
    'cardio': procedimentos_cardio
}

query_fenix = Template(
    f"""
    DECLARE dataset_positivo_count INT64;

    -- PROCEDIMENTOS REALIZADOS (GERAL)
    CREATE OR REPLACE TEMP TABLE temp_sinistros_unificados AS
    SELECT * FROM (
        SELECT DISTINCT idPessoa,
        MAX(idadePessoa) as idade,
        idProcedimento,
        dtAtendimentoContaPaga
        FROM `{ETL_INPUT_TABLE}`
        WHERE idprocedimento IN ($procedimentos)
        GROUP BY idPessoa,idProcedimento,dtAtendimentoContaPaga
    );
    
    ------------------------------------------------------------------------------------------------------------
    
    -- IDS POSITIVOS
    CREATE OR REPLACE TEMP TABLE temp_ids_positivos AS
    SELECT DISTINCT 
        idPessoa, 
        MAX(dtInicioAtendimento) AS dtIncioInternacao
    FROM `{ETL_INPUT_TABLE}`
    WHERE $string_flag_especialidade
    GROUP BY idpessoa;
    
    ------------------------------------------------------------------------------------------------------------
    
    -- DATASET POSITIVO
    -- Buscamos ocorrências de procedimentos de interesse nos registros de todos os pacientes que tiveram desfecho positivo.
    -- Timeline da busca de procedimentos: 6 meses de procedimentos -> gap de 3 meses -> desfecho.
    -- A razão pela qual deixamos um gap entre o período de busca e a data de desfecho, é porque queremos avaliar a capacidade
    -- de detecção precoce do modelo. Além disso, queremos diminiuir a correlação do desfecho com procedimentos obrigatórios 
    -- e aumentar a aprendizagem de procedimentos indicativos de condição médica que pode levar ao desfecho.
    
    CREATE OR REPLACE TEMP TABLE temp_dataset_positivo AS
    SELECT temp_sinistros_unificados.idpessoa,
    temp_sinistros_unificados.idprocedimento,
    temp_sinistros_unificados.idade
    FROM temp_sinistros_unificados JOIN temp_ids_positivos
        ON temp_sinistros_unificados.IdPessoa = temp_ids_positivos.IdPessoa
    WHERE dtAtendimentoContaPaga < DATE_ADD(dtIncioInternacao, INTERVAL -3 MONTH) AND
            dtAtendimentoContaPaga >= DATE_ADD(dtIncioInternacao, INTERVAL -9 MONTH);
    
    ------------------------------------------------------------------------------------------------------------
    
    -- IDS NEGATIVOS (2019)
    SET dataset_positivo_count = (SELECT COUNT (DISTINCT idpessoa) FROM temp_ids_positivos);
    
    CREATE OR REPLACE TEMP TABLE temp_ids_negativos AS
    SELECT ts.idpessoa
           , MAX(dtAtendimentoContaPaga) AS max_dtAtendimentoContaPaga,
           ts.idade
    FROM temp_sinistros_unificados ts
    LEFT JOIN temp_ids_positivos ip
    ON ts.idpessoa = ip.idpessoa
    WHERE EXTRACT(YEAR FROM dtAtendimentoContaPaga) = 2019
    AND ip.dtIncioInternacao is null
    GROUP BY ts.idpessoa, ip.dtIncioInternacao,ts.idade;
    
    CREATE OR REPLACE TEMP TABLE temp_ids_negativos AS
    SELECT * FROM temp_ids_negativos
    WHERE RAND() < dataset_positivo_count / (SELECT COUNT(*) FROM temp_ids_negativos);
    
    ------------------------------------------------------------------------------------------------------------
    
    -- DATASET NEGATIVO (2019)
    CREATE OR REPLACE TEMP TABLE temp_dataset_negativo AS
    SELECT sinistros_unificados.idpessoa, sinistros_unificados.idprocedimento,sinistros_unificados.idade
    FROM temp_sinistros_unificados AS sinistros_unificados JOIN temp_ids_negativos AS ids_negativos
        ON sinistros_unificados.IdPessoa = ids_negativos.IdPessoa
    WHERE dtAtendimentoContaPaga < max_dtAtendimentoContaPaga AND
            dtAtendimentoContaPaga >= DATE_ADD(max_dtAtendimentoContaPaga, INTERVAL -6 MONTH);
            
    ------------------------------------------------------------------------------------------------------------
    
    --SELECT idPessoa, idade, idProcedimento, 'positivo' AS dataset_type FROM ------temp_dataset_positivo
   -- UNION ALL
    --SELECT idPessoa, idade,idProcedimento, 'negativo' AS dataset_type FROM temp_dataset_negativo
   
   
        CREATE OR REPLACE TEMP TABLE df_unificado AS
    SELECT idPessoa, idProcedimento,idade, 'positivo' AS dataset_type FROM temp_dataset_positivo
    UNION ALL
    SELECT idPessoa, idProcedimento,idade, 'negativo' AS dataset_type FROM temp_dataset_negativo;
	
	CREATE OR REPLACE TEMP TABLE cronicidades AS
	SELECT idPessoa,
    MAX(CronicoDiabetes_flag) AS diabetes,
    MAX(CronicoDislipidemia_flag) AS dislipidemia,
    MAX(CronicoHipertensaoArterial_flag) AS hipertensao,
    MAX(CronicoCardiopatias_Flag) AS cardiopatia,
    MAX(CronicoReumatologico_Flag) AS reumatologia,
from fenix_bi.view_fato_classificacao_vidas group by idPessoa; 

SELECT df_unificado.idPessoa,df_unificado.idProcedimento,df_unificado.idade, df_unificado.dataset_type, 
diabetes,dislipidemia,hipertensao,cardiopatia,reumatologia from df_unificado inner join cronicidades  on df_unificado.idPessoa=cronicidades.idPessoa   
   
   
   """
)

query_bariatrica_fenix = query_fenix.substitute(
    procedimentos=get_keys_from_dict_as_sql_values(procedimentos_bariatrica),
    string_flag_especialidade="INT_FlagBariatrica = 'Sim'"
)

query_neonatal_fenix = query_fenix.substitute(
    procedimentos=get_keys_from_dict_as_sql_values(procedimentos_neonatal),
    string_flag_especialidade="INT_FlagNeoNatal = 'Sim'"
)

query_bucomaxilo_fenix = query_fenix.substitute(
    procedimentos=get_keys_from_dict_as_sql_values(procedimentos_bucomaxilo),
    string_flag_especialidade="INT_IdPassagem_INTernacaoPrincipalBucomaxilofacial IS NOT NULL"
)

query_cardio_fenix = query_fenix.substitute(
    procedimentos=get_keys_from_dict_as_sql_values(procedimentos_cardio),
    string_flag_especialidade="INT_IdPassagem_INTernacaoPrincipalCardiovascular IS NOT NULL"
)

query_ortopedia_fenix = query_fenix.substitute(
    procedimentos=get_keys_from_dict_as_sql_values(procedimentos_ortopedia),
    string_flag_especialidade="INT_IdPassagem_INTernacaoPrincipalOrtopedia IS NOT NULL"
)

queries_fenix_by_model = {
    'bariatrica': query_bariatrica_fenix,
    'neonatal': query_neonatal_fenix,
    'ortopedia': query_ortopedia_fenix,
    'bucomaxilo': query_bucomaxilo_fenix,
    'cardio': query_cardio_fenix
}