# -*- coding: utf-8 -*-
import pandas as pd
from google.cloud import bigquery
from src.etl.etl_utils import procedimentos_by_model, queries_fenix_by_model
from src.config.config import GCLOUD_CREDENTIALS_FILEPATH, GETL_DATA_FILEPATH


class ETL:
    """Classe responsável por todos os processos de ETL do dataset que será passado para o modelo preditivo."""

    def __init__(self, model_name):
        self.model_name = model_name
        self.positive_data = None
        self.negative_data = None
        self.final_dataset = None

    def extract(self):
        #columns = ['idPessoa', 'idProcedimento','faixa_etaria', 'dataset_type']

        print(f"Extraindo dados do GETL.")
        #getl_data = pd.read_hdf(GETL_DATA_FILEPATH, key=self.model_name, columns=columns)
        
        print(f"Extraindo dados do BigQuery.")
        client = bigquery.Client.from_service_account_json(json_credentials_path=GCLOUD_CREDENTIALS_FILEPATH)
        print(f"Extraindo dados do BigQuery.")
        

        bq_data = client.query(queries_fenix_by_model[self.model_name]).to_dataframe(progress_bar_type='tqdm')
 
        #bq_data.columns = columns

        print("Unindo datasets do GETL e BigQuery")
        #raw_data = pd.concat([getl_data, bq_data])
        raw_data = bq_data.copy()
        #print(raw_data.head(3))
        
        print(f"Separando amostras positivas.")
        self.positive_data = raw_data[raw_data['dataset_type'] == 'positivo']
        


        print(f"Separando amostras negativas.")
        self.negative_data = raw_data[raw_data['dataset_type'] == 'negativo']
        

    @staticmethod
    def transform(extracted_df, model_name):
        """Método responsável pela transformação de um dataframe "raw" em um dataframe aceito pelo modelo.

         O método consiste de 3 passos:
            1. Agrupa os dados do `extracted_df` por pessoa.
            2. categoriza os procedimentos selecionados de acordo com o `hash_procedimentos_descricao`.
            3. contabiliza a quantidade de procedimentos realizados em cada categoria.

        Params:
            extracted_df: dataset em formato raw.

        Returns:
            transformed_df: dataset em formato clean.
        """
        extra_cols = list(extracted_df.columns)
        extra_cols.remove('idPessoa')
        extra_cols.remove('idProcedimento')
        
        if len(extra_cols) > 0:
            transformed_df = extracted_df.drop(columns=extra_cols).copy()
            extra_cols.append('idPessoa')
            aux_df = extracted_df[extra_cols].copy()
            #print(aux_df.head(3))
        else:
            transformed_df = extracted_df.copy()
            

        # Mapeamento id -> descrição de procedimentos.
        transformed_df['idProcedimento'] = transformed_df['idProcedimento']\
            .map(procedimentos_by_model[model_name])\
            .fillna('outros')

        # Contagem de procedimentos selecionados por pessoa.
        transformed_df = transformed_df.groupby(['idPessoa', 'idProcedimento'])\
            .agg(count=('idProcedimento', 'count'))

        # Formatação do dataframe resultante para ficar no padrão definido.
        transformed_df = transformed_df.reset_index()\
            .pivot(index='idPessoa', columns='idProcedimento', values='count')\
            .rename_axis('', axis='columns')\
            .drop(columns=['outros'], errors='ignore')\
            .fillna(0)
        
        if len(extra_cols)> 0:
            transformed_df = transformed_df.merge(aux_df,'left','idPessoa')
            #transformed_df = pd.concat([transformed_df,aux_df])
            
            #transformed_df = transformed_df.set_index('idPessoa') 
            transformed_df = transformed_df.drop_duplicates(subset=['idPessoa'])
            transformed_df = transformed_df.set_index('idPessoa') 
        
        

        return transformed_df

    def run(self):
        # Extract.
        self.extract()

        # Transform.
        self.positive_data = self.transform(self.positive_data, self.model_name)
        self.negative_data = self.transform(self.negative_data, self.model_name)

        # Label.
        self.positive_data['target'] = 1
        self.negative_data['target'] = 0
 
        # Unify both positive and negative datasets.
        self.final_dataset = pd.concat([self.positive_data, self.negative_data])
        print(self.final_dataset.head(15))
        print(self.final_dataset.shape)
        print(self.final_dataset.target.value_counts())
        print(self.final_dataset.columns)
        

        return self.final_dataset