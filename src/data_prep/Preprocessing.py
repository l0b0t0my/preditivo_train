import itertools
import numpy as np


class Preprocessing:
    @staticmethod
    def clean_dataset(in_df):
        out_df = in_df.replace([np.inf, -np.inf, np.nan], 0)
        return out_df

    @staticmethod
    def combine_features(in_df):
        out_df = in_df.copy()
        features = out_df.drop(['target'], axis=1, errors='ignore').columns

        comb_features = list(itertools.combinations(features, 2)) + \
                        list(itertools.combinations(features, 3)) + \
                        list(itertools.combinations(features, 4))

        for combination in comb_features:
            out_df[combination] = 0.0
            out_df[combination] = out_df[list(combination)].all(axis=1).astype(float)

        return out_df

    def run(self, in_df):
        #preprocessed_df = self.combine_features(in_df)
        preprocessed_df = self.clean_dataset(in_df)
        return preprocessed_df