import os
from joblib import dump
from src.config.config import MODELS_DIR, GCLOUD_BUCKET, PREDITIVOS_FOLDER_NAME, MODEL_NAME
from src.utils.gcp_utils import upload_file
from src.utils.evaluation import metrics_report
from src.utils.evaluation_cv import cv_report
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from src.training.FeatureSelection import FeatureSelection


class TrainingPipeline:
    def __init__(self, model_name):
        self.fs = None
        self.clf = None
        self.model_name = model_name

    def run(self, in_df):
        # Separar dados de treino e teste.
        print('Executando split treino-teste.')
        X = in_df.drop(columns=['target','dataset_type']).copy()
        
        #print(X.columns)
        
        y = in_df['target'].copy()
        

        X_train, X_test, y_train, y_test = train_test_split(
            X, y,
            test_size=0.3,
            random_state=42
        )

        print(f'Tamanho do treino: {len(y_train)} amostras.')
        print(f'Tamanho do teste: {len(y_test)} amostras.')

        # Executar seleção de features.
        print('Iniciando seleção de features.')
        self.fs = FeatureSelection()
        selected_features = self.fs.run(X_train, y_train)
        selected_X_train = X_train[selected_features]
        selected_X_test = X_test[selected_features]

        # Inicializar modelo.
        self.clf = LogisticRegression(
            penalty='none',
            n_jobs=-1,
            random_state=42
        )

        # Treinar modelo.
        print('Iniciando treinamento do modelo.')
        self.clf.fit(X_train, y_train)

        # Salvar modelo com features selecionadas em um dicionário.
        model_struct = {
            'model': self.clf,
            'features': X_train
        }

        LOCAL_MODEL_FILEPATH = f'{MODELS_DIR}/{self.model_name}/{MODEL_NAME}'
        os.makedirs(os.path.dirname(LOCAL_MODEL_FILEPATH), exist_ok=True)
        dump(model_struct, LOCAL_MODEL_FILEPATH)

        DESTINATION_FILEPATH = f'{PREDITIVOS_FOLDER_NAME}/{self.model_name}/{MODEL_NAME}'
        print(f'Salvando modelo {self.model_name} em {GCLOUD_BUCKET}/{DESTINATION_FILEPATH}')
        upload_file(
            bucket_name=GCLOUD_BUCKET,
            destination_blob_name=DESTINATION_FILEPATH,
            source_file_path=LOCAL_MODEL_FILEPATH
        )

        # Calcular métricas de teste.
        print('Iniciando teste do modelo.')
        y_pred = self.clf.predict(X_test)
        metrics_report(y_test, y_pred, self.model_name)
        cv_report(X_train, y_train, self.clf)


        print('Pipeline de treino finalizado.')

        return self.clf