from sklearn.model_selection import cross_validate, cross_val_score
from sklearn.metrics import confusion_matrix
import numpy as np

def cv_scorer(model, X_train, y_train):
    y_pred = model.predict(X_train)
    cm = confusion_matrix(y_train, y_pred)
    print({'true negative': cm[0, 0], 'false positive': cm[0, 1],
           'false negative': cm[1, 0], 'true positive': cm[1, 1]})
    return 0

def cv_report(X_train, y_train, model):
    cv_cf = cross_validate(model, X_train, y_train, cv=4, scoring=cv_scorer)
    cv_sc = cross_val_score(model, X_train, y_train, cv=4)
    print(np.mean(cv_sc), np.std(cv_sc))