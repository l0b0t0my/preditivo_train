import os
from datetime import datetime

from src.config.config import METRICS_LOG_DIR, METRICS_CM_DIR, METRICS_MONITORING_DIR, PREDITIVOS_FOLDER_NAME
from src.config.config import GCLOUD_BUCKET

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

from sklearn.metrics import f1_score, precision_score, recall_score
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay

from src.utils.gcp_utils import upload_file


def metrics_report(y_test, y_pred, model_name):
    current_date = str(datetime.today().date())

    # Calcular métricas de interesse (F-measure, precision, recall).
    metrics_dict = {
        'fmeasure': f1_score(y_test, y_pred),
        'precision': precision_score(y_test, y_pred),
        'recall': recall_score(y_test, y_pred)
    }
    metrics_df = pd.DataFrame(data=metrics_dict, index=pd.Series(current_date, name='date'))
    print("Métricas:")
    print(metrics_df)

    # Plotar e salvar gráfico da matriz de confusão.
    CM_PATH = f'{METRICS_CM_DIR}/{model_name}'
    CM_FILEPATH = f'{CM_PATH}/{current_date}.png'
    os.makedirs(os.path.dirname(CM_FILEPATH), exist_ok=True)

    ConfusionMatrixDisplay(confusion_matrix(y_test, y_pred)).plot()
    plt.title(f'Treino realizado em {current_date}.')
    plt.savefig(fname=CM_FILEPATH)
    plt.show()

    # Salvar métricas desse teste no arquivo de log.
    LOG_PATH = f'{METRICS_LOG_DIR}/{model_name}/metrics_log.csv'
    os.makedirs(os.path.dirname(LOG_PATH), exist_ok=True)

    try:
        metrics_log_df = pd.read_csv(LOG_PATH, sep=';', index_col='date')
        metrics_log_df = metrics_log_df.append(metrics_df)
    except FileNotFoundError:
        metrics_log_df = metrics_df

    metrics_log_df.to_csv(LOG_PATH, sep=';')

    DESTINATION_FILEPATH = f'{PREDITIVOS_FOLDER_NAME}/{model_name}/metrics.csv'
    print(f'Salvando log de métricas em {GCLOUD_BUCKET}/{DESTINATION_FILEPATH}')
    upload_file(
        bucket_name=GCLOUD_BUCKET,
        destination_blob_name=DESTINATION_FILEPATH,
        source_file_path=LOG_PATH
    )

    # Criar gráfico de monitoramento com as métricas do arquivo de log.
    sns.set(rc={'figure.figsize': (20, 10)})

    ax = sns.lineplot(data=metrics_log_df, markers=True)
    ax.set(title='Métricas Ao Longo Do Tempo')
    ax.get_yaxis().set_visible(False)

    for col in metrics_log_df.columns:
        for x, y in zip(metrics_log_df.index, metrics_log_df[col]):
            plt.text(
                x=x,
                y=y + 0.001,
                s=f'{(100 * y):.0f}%',
                horizontalalignment='center'
            )

    MONITORING_PATH = f'{METRICS_MONITORING_DIR}/{model_name}/metrics_plot.png'
    os.makedirs(os.path.dirname(MONITORING_PATH), exist_ok=True)
    plt.savefig(fname=MONITORING_PATH)
    plt.show()

    # TODO: salvar gráfico de monitoramento no bucket.
    # TODO: salvar métricas no BigQuery.
